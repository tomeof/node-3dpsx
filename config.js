const config = {
  port: 9000,
  boundingBoxBufferPercent: 15,
  tempLayers: {
    expiry: {
      seconds: 86400
    }
  }
}

module.exports = config;