const express = require("express");
const pug = require("pug");

let router = express.Router();

router.route("/")
  .get((req, res) => {

    let art = `
      \\_/       .:"    .:"    .:"
    -=(_)=-  /\\||   /\\||   /\\||
      / \\   //\\\\|  //\\\\|  //\\\\|
           //  \\\\ //  \\\\ //  \\\\
          //    \\^/    \\^/    \\\\
          |[]  []|[]  []|[]  []|
         &|  ||  %  ||  |  ||  |%
      &%&--==--&%-==--%&"""""%&%""""
`;

    const compiledFunction = pug.compileFile("./views/home.pug");
    let home = compiledFunction({art: art});
    res.set("Content-Type", "text/html");
    res.send(home);
    
  }); 

module.exports = router;