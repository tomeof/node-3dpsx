const express = require("express");
const { check, validationResult } = require("express-validator")
const { toLower } = require("../src/customSanitizers");
const { errorHandler } = require("../src/errorHandler");
const checkFactory = require("../src/validationCheckFactory");
const { getSceneHandler } = require("../src/getSceneHandler");
const { getCapabilitiesHandler } = require("../src/getCapabilitiesHandler");
const assets = require("../src/assets");
const bb = require("../src/boundingbox");
const redirectTo = require("../src/redirectTo");

let router = express.Router();

const checks = [
  ...checkFactory.commonChecks(),
  check("request")
    .exists().withMessage("MissingParameterValue")
    .customSanitizer(toLower)
    .isIn(["getscene", "getcapabilities"]).withMessage("InvalidParameterValue")
];

router.route("/").get(checks, (req, res) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    let exception = errorHandler(errors);
    res.set("Content-Type", "application/xml");
    return res.status(400).send(exception);
  }

  redirectTo(req.query.request, req, res);

});

const capabilitiesChecks = [
  check("request")
    .exists().withMessage("MissingParameterValue")
    .customSanitizer(toLower)
    .equals("getcapabilities").withMessage("InvalidParameterValue")
];

router.route("/capabilities").get(capabilitiesChecks, (req, res) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    let exception = errorHandler(errors);
    res.set("Content-Type", "application/xml");
    return res.status(400).send(exception);
  }

  getCapabilitiesHandler(req, res);

});

const sceneChecks = [
  check("request")
    .exists().withMessage("MissingParameterValue")
    .customSanitizer(toLower)
    .equals("getscene").withMessage("InvalidParameterValue"),
  check("layers")
    .exists().withMessage("MissingParameterValue")
    .custom(value => assets.allLayersExist(value)).withMessage("UnknownLayer"),
  check("boundingbox")
    .optional()
    .matches(/^((\-?\d+(\.\d+)?),){3}(\-?\d+(\.\d+)?)$/).withMessage("InvalidParameterValue")
    .custom(value => bb.isValid(value)).withMessage("InvalidParameterValue"),
  check("cullingvolume")
    .exists().withMessage("MissingParameterValue")
    .matches(/^((\-?\d+(\.\d+)?),){23}(\-?\d+(\.\d+)?)$/).withMessage("InvalidParameterValue"),
  check("camera")
    .exists().withMessage("MissingParameterValue")
    .matches(/^((\-?\d+(\.\d+)?),){5}(\-?\d+(\.\d+)?)$/).withMessage("InvalidParameterValue"),
  check("frustum")
    .exists().withMessage("MissingParameterValue")
    .matches(/^((\-?\d+(\.\d+)?),){2}(\-?\d+(\.\d+)?)$/).withMessage("InvalidParameterValue"),
  check("drawingbuffer")
    .exists().withMessage("MissingParameterValue")
    .matches(/^((\-?\d+(\.\d+)?),){1}(\-?\d+(\.\d+)?)$/).withMessage("InvalidParameterValue"),
  check("time")
    .exists().withMessage("MissingParameterValue")
    .matches(/\d+/).withMessage("InvalidParameterValue")
];

router.route("/scene").get(sceneChecks, (req, res) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    let exception = errorHandler(errors);
    res.set("Content-Type", "application/xml");
    return res.status(400).send(exception);
  }

  getSceneHandler(req, res);

});

module.exports = router;