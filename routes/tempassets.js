const express = require("express");
const bodyParser = require("body-parser");
const tempAssets = require("../src/tempAssets");

let router = express.Router();

router.route("/")
  .post(bodyParser.json(), (req, res, next) => {

    tempAssets.addTempLayer(req, res);

  });

module.exports = router;