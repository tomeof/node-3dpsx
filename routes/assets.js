const express = require("express");
const path = require("path");
const assets = require("../src/assets");

let router = express.Router();

router.route("/*")
  .get((req, res, next) => {

    let fullUrl = `${req.protocol}://${req.get("host")}${req.originalUrl}`;
    assets.isPublic(fullUrl).then(result => {
      if (result) {
        next();
      } else {
        return res.status(403).end("403 Forbidden")
      }
    });

  },

    express.static(path.join(__dirname, "../assets"))

  );

module.exports = router;