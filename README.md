# Node 3D Portrayal Service extended 

![](figures/home.jpg)

This is a 3D Portrayal Service implementation that is compatible with the [I3S](https://github.com/Esri/i3s-spec) specification and  performs an active node selection.


## Installing

The service is implemented using [Node js](https://nodejs.org/en/). Make sure you have Node js (version 10 and above) already installed. Clone or download the repository. Install the dependencies with the `npm install` command. For the moment there is no bundler. Place the contents of the repository on your server with Node.js installed and run the "start" script with npm.

`npm run start`

If your server has a process manager installed, then configure accordingly.

## Usage

You can change the port in `config.js`, default port is 9000.
You access the service with the `/service/v1` endpoint and then provide additional query parameters:

* getScene: `{host}:{port}/service/v1?`
* getScene query parameters:

`service=3DPS`

`&acceptversions=1.0`

`&request=getScene`

`&boundingbox=lon_1,lat_1,lon_2,lat_2`

`&cullingvolume=vec3_plane1_normal,plane1_distance` (x6)

`&camera=vec3_position,vec3_direction`

`&frustum=near_offset,top_offset,right_offset`

`&drawingbuffer=buffer_width,buffer_height`

`&time=time_of_request`

\* when vec3 is prefixed in a query parameter it should be replaced by three comma separated actual values, e.g. x,y,z

When the query parameters are not valid an xml exception is returned.

## Examples

* [getScene](http://81.169.187.7:9001/service/v1?service=3DPS&acceptversions=1.0&request=GetScene&layers=New_York_LoD2_3D_Buildings&boundingbox=-74.0189883681,40.7007424914,-73.9727649425,40.7318181023&cullingvolume=0.9739208992844022,0.22183197873838273,0.04763040148975446,-466686.908510136,-0.974414929031256,-0.21913033649100758,-0.04997441055529999,489653.71773500636,-0.2010488324103693,0.7002772846908004,0.6849752488466331,694036.5778396796,0.2003687953021454,-0.6979086335828746,-0.6875870017966105,-671263.22796277,-0.2094656714218098,0.7295941481510193,-0.6510119134697911,6378671.608204631,0.2094656714218098,-0.7295941481510193,0.6510119134697911,493621327.3917954&camera=1338074.3495312775,-4660674.031130781,4144304.2912002187,-0.2094656714218098,0.7295941481510193,-0.6510119134697911&frustum=1,0.5622205451542581,0.5773502691896256&drawingbuffer=954,929&time=1586730641723)

## Demo

[http://81.169.187.7:9001](http://81.169.187.7:9001)