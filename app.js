const express = require("express");
const config = require("./config");
const port = config.port || 9000;
const home = require("./routes/home");
const v1 = require("./routes/v1");
const assets = require("./routes/assets");
const tempassets = require("./routes/tempassets");
const cors = require("./src/cors");

const app = express();
app.use(cors);
app.use("/", home);
app.use("/service/v1", v1);
app.use("/assets", assets);
app.use("/tempassets", tempassets);

app.listen(port, err => {
  if (err) {
    return console.log(err);
  }
});