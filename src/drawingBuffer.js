class DrawingBuffer {

  constructor(width, height) {
    this.width = width;
    this.height = height;
  }

}

const fromString = (str) => {
  let tokens = str.split(",");
  return new DrawingBuffer(...tokens.map(token => parseFloat(token)));
}

module.exports = { DrawingBuffer, fromString };