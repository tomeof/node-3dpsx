const Plane = require("./plane");
const { Vector3 } = require("./vector");
const Intersect = require("./intersect");

class CullingVolume {

  constructor(planes) {
    this.planes = planes;
  }

  computeVisibility(boundingSphere) {
    let intersecting = false;

    for (let i = 0, len = this.planes.length; i < len; i++) {
      let result = boundingSphere.intersectPlane(this.planes[i]);
      if (result == Intersect.OUTSIDE) {
        return Intersect.OUTSIDE;
      } else if (result == Intersect.INTERSECTING) {
        intersecting = true;
      }

    }

    return intersecting ? Intersect.INTERSECTING : Intersect.INSIDE;
  }

}

const fromString = (str) => {

  let tokens = str.split(",");
  let planes = [];
  for (let i = 0, len = tokens.length; i < len; i += 4) {
    planes.push(new Plane(
      new Vector3(
        parseFloat(tokens[i]),
        parseFloat(tokens[i + 1]),
        parseFloat(tokens[i + 2])
      ),
      parseFloat(tokens[i + 3])
    ));
  }
  return new CullingVolume(planes);
}

module.exports = { CullingVolume, fromString };