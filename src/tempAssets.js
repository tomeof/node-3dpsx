const fs = require('fs').promises;
const { v4: uuidv4 } = require('uuid');
const config = require("../config");
const fetch = require("node-fetch");


const addTempLayer = (req, res) => {


  (async () => {

    try {

      await removeExpired();

      let doc = await fetch(req.body.url);
      doc = await doc.json();


      let tempLayer = {
        name: `(tmp) ${req.body.name}`,
        id: uuidv4(),
        boundingbox: doc.store.extent,
        url: req.body.url,
        creationTime: Date.now(),
        scope: "public"
      };

      let tempassetsJson = await fs.readFile('./assets/temporaryAssets.json', 'utf8');
      let tempassets = JSON.parse(tempassetsJson);
      tempassets.push(tempLayer);
      let updatedTempassets = JSON.stringify(tempassets, null, 2);
      await fs.writeFile('./assets/temporaryAssets.json', updatedTempassets, 'utf8');
      res.status(200).send("layer added");

    } catch (error) {

      console.log(error);
      res.status(400).send(error);

    }

  })();


}

const removeExpired = async () => {

  try {

    let tempassetsJson = await fs.readFile('./assets/temporaryAssets.json', 'utf8');
    let tempassets = JSON.parse(tempassetsJson);

    let currentTime = Date.now();
    let nonExpired = tempassets.filter(tempasset => {
      let diffInSeconds = (currentTime - tempasset.creationTime) / 1000;
      return (diffInSeconds < config.tempLayers.expiry.seconds);
    });
    let updatedTempassets = JSON.stringify(nonExpired, null, 2);
    await fs.writeFile('./assets/temporaryAssets.json', updatedTempassets, 'utf8');

  } catch (error) {

    console.log(error);

  }

}

const getTempAssets = async () => {

  try {

    await removeExpired();
    let tempassetsJson = await fs.readFile('./assets/temporaryAssets.json', 'utf8');
    let tempassets = JSON.parse(tempassetsJson);
    return tempassets;
    
  } catch (error) {

    console.log(error);
    
  }

}


module.exports = { addTempLayer, getTempAssets };