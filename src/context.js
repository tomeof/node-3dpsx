const cullingVolume = require("./cullingVolume");
const gbb = require("./geographicBoundingBox");
const camera = require("./camera");
const offCenterFrustum = require("./offCenterFrustum");
const drawingBuffer = require("./drawingBuffer");

const fromRequest = (req) => {

  return {
    cullingVolume: cullingVolume.fromString(req.query.cullingvolume),
    bb: gbb.fromString(req.query.boundingbox),
    camera: camera.fromString(req.query.camera),
    offCenterFrustum: offCenterFrustum.fromString(req.query.frustum),
    drawingBuffer: drawingBuffer.fromString(req.query.drawingbuffer),
    requestTime: req.query.time
  };

}

module.exports = { fromRequest };