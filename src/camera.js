const vector = require("./vector");

class Camera {

  constructor(position, direction) {
    this.position = position;
    this.direction = direction;
  }

}

const fromString = (str) => {
  let tokens = str.split(",");
  let position = vector.fromString(`${tokens[0]},${tokens[1]},${tokens[2]}`);
  let direction = vector.fromString(`${tokens[3]},${tokens[4]},${tokens[5]}`);
  return new Camera(position, direction);
}

module.exports = { Camera, fromString };