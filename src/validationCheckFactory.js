const { check } = require("express-validator");
const {toLower} = require("../src/customSanitizers");
const assets = require("../src/assets");
const bb = require("../src/boundingbox");

const commonChecks = () => {
  return [
    check("service")
      .exists().withMessage("MissingParameterValue")
      .customSanitizer(toLower)
      .equals("3dps").withMessage("InvalidParameterValue"),
    check("acceptversions")
      .exists().withMessage("MissingParameterValue")
      .equals("1.0").withMessage("InvalidParameterValue")
  ];

}

const specificChecks = (parameter) => {

  switch (parameter.toLowerCase()) {
    case "getscene": {
      return [
        check("boundingbox")
          .exists().withMessage("MissingParameterValue")
          .matches(/^((\-?\d+(\.\d+)?),){3}(\-?\d+(\.\d+)?)$/).withMessage("InvalidParameterValue")
          .custom(value => bb.isValid(value)).withMessage("InvalidParameterValue"),
        check("layers")
          .exists().withMessage("MissingParameterValue")
          .custom(value => assets.allLayersExist(value)).withMessage("UnknownLayer")
      ];
    }
    case "getcapabilities": {
      return [];
    }
  }

}

module.exports = { commonChecks, specificChecks };