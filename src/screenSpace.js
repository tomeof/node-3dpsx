
const screenSpaceError = (context, bs) => {

  let c = context.camera;
  let frustum = context.offCenterFrustum;
  let buffer = context.drawingBuffer;

  let cameraToSphere = bs.center.subtract(c.position);

  let cameraToSphereProj = c.direction.clone();
  cameraToSphereProj.multiplyScalar(c.direction.dot(cameraToSphere));
  let distance = cameraToSphereProj.magnitude();
  let inverseNear = 1.0 / frustum.near;
  let tanTheta = frustum.top * inverseNear;
  let pixelHeight = 2.0 * distance * tanTheta / buffer.height;
  tanTheta = frustum.right * inverseNear;
  let pixelWidth = 2.0 * distance * tanTheta / buffer.width;
  return Math.max(bs.diameter / pixelWidth, bs.diameter / pixelHeight);
}

module.exports = { screenSpaceError };