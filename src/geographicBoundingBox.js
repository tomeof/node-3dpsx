const boxIntersect = require("box-intersect");
const angle = require("./angle");

class GeographicBoundingBox {

  constructor(minLong, minLat, maxLong, maxLat) {
    this.minLong = minLong;
    this.minLat = minLat;
    this.maxLong = maxLong;
    this.maxLat = maxLat;
  }

  extend(percent) {
    percent *= 0.01;
    let diffLon = (this.maxLong - this.minLong) * percent;
    let diffLat = (this.maxLat - this.minLat) * percent;
    this.minLong -= diffLon;
    this.maxLong += diffLon;
    this.minLat -= diffLat;
    this.maxLat += diffLat;
  }

  getCorners() {

    return [
      this.minLong,
      this.minLat,
      this.maxLong,
      this.maxLat
    ];

  }

  intersectWith(gbb) {

    let overlaps = boxIntersect([
      this.getCorners(),
      gbb.getCorners()
    ]);

    if (overlaps.length > 0) {
      return true;
    }
    return false;
  }

  intersectWithGeographic(geographic) {
    if (this.minLong <= geographic.longitude &&
      this.maxLong >= geographic.longitude &&
      this.minLat <= geographic.latitude &&
      this.maxLat >= geographic.latitude) {
      return true;
    }
    return false;
  }

  clone() {
    return new GeographicBoundingBox(...this.getCorners());
  }

}

const fromString = (str) => {
  if(!str) return null;
  let tokens = str.split(",");
  return new GeographicBoundingBox(...tokens.map(token => parseFloat(token)));
}

const fromBoundingSphere = (boundingSphere) => {
  let center = boundingSphere.geographicCenter;
  let deltaLat = angle.roughDistanceToLatitude(boundingSphere.radius);
  let deltaLong = angle.roughDistanceToLongitude(center.latitude, boundingSphere.radius);
  return new GeographicBoundingBox(
    center.longitude - deltaLong,
    center.latitude - deltaLat,
    center.longitude + deltaLong,
    center.latitude + deltaLat
  );
}

module.exports = { GeographicBoundingBox, fromString, fromBoundingSphere };