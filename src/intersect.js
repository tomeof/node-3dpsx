const Intersect = {
  INSIDE: 0,
  OUTSIDE: 1,
  INTERSECTING: 2
}

module.exports = Intersect;