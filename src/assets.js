const fs = require("fs");
const fsprom = require('fs').promises;
const boxIntersect = require("box-intersect");
const path = require('path');
const tempAssets = require("../src/tempAssets");

// const getLayers = (layers) => {
//   return new Promise((resolve, reject) => {
//     fs.readFile("./assets/assets.json", (err, data) => {
//       if (err) {
//         reject();
//       }
//       let assets = JSON.parse(data);
//       let requestedLayers = layers.split(",");

//       assets = assets.filter(asset => requestedLayers.includes(asset.id));
//       assets = assets.filter(asset => asset.scope == "public");
//       resolve(assets);
//     });
//   });
// }

const getLayers = async (layers) => {

  let assets = await getAllAssets();
  let requestedLayers = layers.split(",");
  assets = assets.filter(asset => requestedLayers.includes(asset.id));
  assets = assets.filter(asset => asset.scope == "public");
  return assets;

}

const getAllAssets = async () => {

  try {

    let assetsJson = await fsprom.readFile('./assets/assets.json', 'utf8');
    let assets = JSON.parse(assetsJson);
    let tempassets = await tempAssets.getTempAssets();
    assets.push(...tempassets);
    return assets;

  } catch (error) {

    console.log(error);
    return error;

  }

}

const getIntersectingLayers = (bb, layers) => {

  return new Promise((resolve, reject) => {
    fs.readFile("./assets/assets.json", (err, data) => {
      if (err) {
        reject();
      }
      let assets = JSON.parse(data);
      let requestedBB = bb.split(",").map(token => parseFloat(token));
      let requestedLayers = layers.split(",");

      assets = assets.filter(asset => requestedLayers.includes(asset.id));
      assets = assets.filter(asset => asset.scope == "public");

      assets = assets.filter(asset => {

        let overlaps = boxIntersect([
          [...asset.boundingbox],
          [...requestedBB]
        ]);

        if (overlaps.length > 0) {
          return true;
        }
        return false;
      });

      resolve(assets);

    });
  });

}

const getIntersecting = (bb) => {

  return new Promise((resolve, reject) => {
    fs.readFile("./assets/assets.json", (err, data) => {
      if (err) {
        reject();
      }
      let assets = JSON.parse(data);
      let requestedBB = bb.split(",").map(token => parseFloat(token));

      assets = assets.filter(asset => asset.scope == "public");
      assets = assets.filter(asset => {

        let overlaps = boxIntersect([
          [...asset.boundingbox],
          [...requestedBB]
        ]);

        if (overlaps.length > 0) {
          return true;
        }
        return false;
      });

      resolve(assets);

    });
  });

}

// const allLayersExist = (layers) => {

//   if (!layers) return Promise.reject();

//   return new Promise((resolve, reject) => {
//     fs.readFile("./assets/assets.json", (err, data) => {
//       if (err) {
//         reject();
//       }
//       let assets = JSON.parse(data);
//       layers = layers.split(",");
//       layers.forEach(layer => {
//         if (assets.findIndex(asset => asset.id == layer) == -1) {
//           reject();
//         }
//       });
//       resolve();
//     });
//   });

// }

const allLayersExist = async (layers) => {

  if (!layers) return Promise.reject();
  let assets = await getAllAssets();
  layers = layers.split(",");
  layers.forEach(layer => {
    if (assets.findIndex(asset => asset.id == layer) == -1) {
      return Promise.reject()
    }
  });
  return;
}

// const getPublicLayers = () => {

//   return new Promise((resolve, reject) => {
//     fs.readFile("./assets/assets.json", (err, data) => {
//       let assets = JSON.parse(data);
//       assets = assets.filter(asset => asset.scope == "public");
//       resolve(assets);
//     });
//   });

// }

const getPublicLayers = async () => {

  let assets = await getAllAssets();
  assets = assets.filter(asset => asset.scope == "public");
  return assets;

}

const isPublic = (url) => {

  return new Promise((resolve, reject) => {
    fs.readFile("./assets/assets.json", (err, data) => {
      let assets = JSON.parse(data);
      assets = assets.filter(asset => {
        let parentDir = path.dirname(asset.url);
        return !path.relative(parentDir, url).startsWith("..");
      });
      if (assets.length == 0) {
        resolve(false);
        return;
      }
      if (!assets[0].scope == "public") {
        resolve(false);
        return;
      }
      resolve(true);
    });
  });

}

module.exports = { getIntersectingLayers, getIntersecting, allLayersExist, getPublicLayers, isPublic, getLayers };