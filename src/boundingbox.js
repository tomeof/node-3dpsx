const isValid = (value) => {

  let tokens = value.split(",");
  tokens = tokens.map(token => parseFloat(token));
  if (tokens[0] < tokens[2] && tokens[1] < tokens[3]) {
    return true;
  }
  return false;

}

module.exports = { isValid };