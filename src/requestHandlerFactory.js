const { getSceneHandler } = require("./getSceneHandler");
const { getCapabilitiesHandler } = require("./getCapabilitiesHandler");

const createHandler = (request) => {

  switch (request.toLowerCase()) {
    case "getscene": {
      return getSceneHandler;
    }
    case "getcapabilities": {
      return getCapabilitiesHandler;
    }
  }

}

module.exports = { createHandler };