const vector = require("./vector");
const Intersect = require("./intersect");
const Geographic = require("./geographic");

class BoundingSphere {

  constructor(geographicCenter, radius) {
    this.geographicCenter = geographicCenter;
    this.center = vector.fromDegrees(geographicCenter.longitude, geographicCenter.latitude, geographicCenter.height);
    this.radius = radius;
  }

  get diameter() {
    return 2.0 * this.radius;
  }

  intersectPlane(plane) {
    let distanceToPlane = plane.normal.dot(this.center) + plane.distance;
    if (distanceToPlane < -this.radius) {
      // The center point is at the negative side of the plane normal
      return Intersect.OUTSIDE;
    } else if (distanceToPlane < this.radius) {
      // The center point is at the positive side of the plane, but radius extends beyond it; partial overlap
      return Intersect.INTERSECTING;
    }
    return Intersect.INSIDE;
  }

  distanseTo(vector) {
    return Math.max(0.0, this.center.distance(vector) - this.radius);
  }

}

const fromArray = (array) => {
  let lng = array[0];
  let lat = array[1];
  let height = array[2];
  let radius = array[3];
  return new BoundingSphere(new Geographic(lng, lat, height), radius);
}

module.exports = { BoundingSphere, fromArray };