class Geographic {

  constructor(longitude, latitude, height) {
    this.longitude = longitude;
    this.latitude = latitude;
    this.height = height || 0.0;
  }

}

module.exports = Geographic;