const pug = require("pug");

const errorHandler = (errors) => {
  let e = null;
  if (errors.array()[0].nestedErrors) {
    e = errors.array()[0].nestedErrors[0];
  } else {
    e = errors.array()[0];
  }

  const compiledFunction = pug.compileFile("./views/exception.pug");
  let exception = compiledFunction({
    exceptionCode: e.msg,
    locator: e.param
  });

  return exception;
}

module.exports = { errorHandler };