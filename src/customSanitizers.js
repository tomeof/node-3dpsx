const toLower = (value) => {
  if(!value) return;
  return value.toLowerCase();
}

module.exports = { toLower };