const assets = require("../src/assets");
const Traverse = require("./traverse");

const getSceneHandler = (req, res) => {

  assets.getLayers(req.query.layers).then(layers => {

    let traverse = new Traverse();
    traverse.begin(req, layers[0].url)
      .then(nodes => {
        res.status(200).json(nodes);
      });

  });

}

module.exports = { getSceneHandler };