const url = require('url');

const redirectTo = (request, req, res) => {

  switch (request.toLowerCase()) {
    case "getscene": {
      res.redirect(
        url.format({
          pathname: `${req.baseUrl}/scene`,
          query: req.query,
        })
      );
      break;
    }
    case "getcapabilities": {
      res.redirect(
        url.format({
          pathname: `${req.baseUrl}/capabilities`,
          query: req.query,
        })
      );
      break;
    }
  }

}

module.exports = redirectTo;