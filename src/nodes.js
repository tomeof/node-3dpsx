const config = require("../config");

class Nodes {

  constructor() {
    this.nodes = [];
  }

  get sorted() {
    return this.nodes.sort((a, b) => a.distanceToCamera > b.distanceToCamera);
  }

  add(context, node, boundingSphere) {

    let entry = {
      id: node.id,
      level: node.level,
      lng: node.mbs[0],
      lat: node.mbs[1],
      radius: node.mbs[3],
      url: `${context.layer}/nodes/${node.id}`,
      distanceToCamera: boundingSphere.distanseTo(context.camera.position),
      time: context.requestTime
    };

    if (context.bb != null) {
      let requestedBB = context.bb.clone();
      requestedBB.extend(config.boundingBoxBufferPercent);
      if (requestedBB.intersectWithGeographic(boundingSphere.geographicCenter)) {
        this.nodes.push(entry);
      }
    } else {
      this.nodes.push(entry);
    }

  }

}

module.exports = Nodes;