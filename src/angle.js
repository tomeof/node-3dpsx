const RADIANS_PER_DEGREE = Math.PI / 180.0;
const METERS_PER_LONGITUDE_DEGREE_AT_EQUATOR = 111321;
const METERS_PER_LATITUDE_DEGREE = 111130;

const degreesToRadians = (degrees) => {
  return degrees * RADIANS_PER_DEGREE;
}

/**
 * Surface is considered planar. This simplifies the calculations.
 */
const roughDistanceToLongitude = (latitude, distance) => {
  let metersPerLongitude = Math.cos(degreesToRadians(latitude)) * METERS_PER_LONGITUDE_DEGREE_AT_EQUATOR;
  return distance / metersPerLongitude;
}

const roughDistanceToLatitude = (distance) => {
  return distance / METERS_PER_LATITUDE_DEGREE;
}

module.exports = { degreesToRadians, roughDistanceToLongitude, roughDistanceToLatitude }