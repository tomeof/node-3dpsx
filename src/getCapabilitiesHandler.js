const assets = require("../src/assets");
const pug = require("pug");

const getCapabilitiesHandler = (req, res) => {

  assets.getPublicLayers().then(layers => {

    const compiledFunction = pug.compileFile("./views/capabilities.pug");
    let capabilities = compiledFunction({ layers: layers });
    res.set("Content-Type", "application/xml");
    res.status(200).send(capabilities);
    
  });

}

module.exports = { getCapabilitiesHandler };