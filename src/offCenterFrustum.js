class OffCenterFrustum {

  constructor(near, top, right) {
    this.near = near;
    this.top = top;
    this.right = right;
  }

}

const fromString = (str) => {
  let tokens = str.split(",");
  return new OffCenterFrustum(...tokens.map(token => parseFloat(token)));
}

module.exports = { OffCenterFrustum, fromString };